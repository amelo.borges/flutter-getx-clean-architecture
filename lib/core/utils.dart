import 'package:logger/logger.dart';

mixin Utils {
  final _logger = Logger(
    printer: PrettyPrinter(
        methodCount: 2, // number of method calls to be displayed
        errorMethodCount: 8, // number of method calls if stacktrace is provided
        lineLength: 120, // width of the output
        colors: true, // Colorful log messages
        printEmojis: true, // Print an emoji for each log message
        printTime: false // Should each log print contain a timestamp
    ),
  );

  loggerVerbose({required dynamic message}) {
    _logger.v(message);
  }

  loggerDebug({required dynamic message}) {
    _logger.d(message);
  }

  loggerInfo({required dynamic message}) {
    _logger.i(message);
  }

  loggerWarning({required dynamic message}) {
    _logger.w(message);
  }

  loggerError({required dynamic message}) {
    _logger.e(message);
  }

  loggerwtf({required dynamic message}) {
    _logger.wtf(message);
  }
}
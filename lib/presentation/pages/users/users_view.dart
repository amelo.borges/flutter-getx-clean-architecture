import 'package:clean_architecture/presentation/pages/users/users_view_model.dart';
import 'package:clean_architecture/presentation/widgets/user_item.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class UsersView extends StatelessWidget {
  const UsersView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<UsersViewModel>(
      builder: (vm) {
        return Scaffold(
          appBar: AppBar(
            title: const Text("Flutter + Getx + Clear Architecture"),
          ),
          body: SizedBox(
            width: double.infinity,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Obx(
                  () => vm.loading.value
                      ? CircularProgressIndicator()
                      : Flexible(
                          child: ListView.builder(
                            shrinkWrap: true,
                            itemCount: vm.userList.length,
                            itemBuilder: (_, index) {
                              final item = vm.userList.elementAt(index);
                              return UserItem(item);
                            },
                          ),
                        ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}

import 'package:clean_architecture/core/utils.dart';
import 'package:clean_architecture/domain/entities/user.dart';
import 'package:clean_architecture/domain/use-cases/get_users.dart';
import 'package:get/get.dart';

class UsersViewModel extends GetxController with Utils{
  late final GetUsersUseCase _getUsers;

  UsersViewModel({required GetUsersUseCase getUsers}) : _getUsers = getUsers;

  final loading = false.obs;
  final _userList = <User>[].obs;
  List<User> get userList => _userList;
  Future getData() async {
    try{
      loading.value = true;
      final response = await _getUsers.allUsers();
      _userList.assignAll(response);
    }catch(e){
      loggerError(message: e);
    }finally{
      loading.value = false;
    }
  }


  @override
  void onInit() {
    getData();
    super.onInit();
  }


}
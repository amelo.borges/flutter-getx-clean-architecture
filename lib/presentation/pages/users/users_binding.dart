import 'package:clean_architecture/data/datasources/user_remote_data_source.dart';
import 'package:clean_architecture/data/datasources/user_remote_data_source_Impl.dart';
import 'package:clean_architecture/data/repositories/user_repository_impl.dart';
import 'package:clean_architecture/domain/repositories/user_repository.dart';
import 'package:clean_architecture/domain/use-cases/get_users.dart';
import 'package:clean_architecture/presentation/pages/users/users_view_model.dart';
import 'package:get/get.dart';

class UsersBinding extends Bindings{

  @override
  void dependencies() {
    Get.lazyPut<UserRemoteDataSource>(() => UserRemoteDataSourceImpl());
    Get.lazyPut<UserRepository>(() => UserRepositoryImpl(userRemoteDataSource: Get.find()));
    Get.lazyPut<GetUsersUseCase>(() => GetUsersUseCase(repository: Get.find()));
    Get.lazyPut<UsersViewModel>(() => UsersViewModel(getUsers: Get.find<GetUsersUseCase>()));
  }

}
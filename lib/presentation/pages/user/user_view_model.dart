import 'package:clean_architecture/core/utils.dart';
import 'package:clean_architecture/domain/entities/user.dart';
import 'package:get/get.dart';

class UserViewModel extends GetxController with Utils{
  final user = Get.arguments as User;
}
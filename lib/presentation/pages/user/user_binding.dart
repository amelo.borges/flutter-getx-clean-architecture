import 'package:clean_architecture/presentation/pages/user/user_view_model.dart';
import 'package:get/get.dart';

class UserBinding extends Bindings{

  @override
  void dependencies() {
    Get.lazyPut(() => UserViewModel());
  }

}
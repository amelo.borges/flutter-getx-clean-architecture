import 'package:clean_architecture/presentation/pages/user/user_binding.dart';
import 'package:clean_architecture/presentation/pages/user/user_view.dart';
import 'package:clean_architecture/presentation/pages/users/users_binding.dart';
import 'package:clean_architecture/presentation/pages/users/users_view.dart';
import 'package:clean_architecture/presentation/routes/routes.dart';
import 'package:get/get.dart';

class AppPages {
  static List<GetPage> pages = [

    GetPage(
        transition: Transition.cupertino,
        name: AppRoutes.users,
        page: ()=> const UsersView(),
        binding: UsersBinding(),
    ),

    GetPage(
        transition: Transition.cupertino,
        name: AppRoutes.userDetail,
        page: ()=> const UserView(),
        binding: UserBinding()
    ),

  ];
}
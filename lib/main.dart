import 'package:clean_architecture/presentation/pages/users/users_binding.dart';
import 'package:clean_architecture/presentation/routes/routes.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'presentation/routes/pages.dart';

void main() {
  runApp(const CleanArchitectureApp());
}

class CleanArchitectureApp extends StatefulWidget {
  const CleanArchitectureApp({Key? key}) : super(key: key);

  @override
  State<CleanArchitectureApp> createState() => _CleanArchitectureAppState();
}

class _CleanArchitectureAppState extends State<CleanArchitectureApp> {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.indigo,
        scaffoldBackgroundColor: const Color.fromRGBO(245,246,252,1.0)
      ),
      initialRoute: AppRoutes.users,
      initialBinding: UsersBinding(),
      getPages: AppPages.pages,
    );
  }
}

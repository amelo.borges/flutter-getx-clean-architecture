import 'package:equatable/equatable.dart';

class User extends Equatable {

  final String createdAt;
  final String name;
  final String email;
  final String city;
  final String avatar;
  final String id;
  final String birthdate;

  const User({
    required this.name,
    required this.avatar,
    required this.email,
    required this.id,
    required this.city,
    required this.createdAt,
    required this.birthdate
  });

  @override
  List<Object?> get props => [
      createdAt,
      name,
      email,
      city,
      avatar,
      id,
      birthdate
  ];

}
import 'package:clean_architecture/domain/entities/user.dart' as entity;
import 'package:clean_architecture/domain/repositories/user_repository.dart';


class GetUsersUseCase {
  final UserRepository _repository;

  GetUsersUseCase({required UserRepository repository}) : _repository =  repository;

  Future<List<entity.User>> allUsers() => _repository.getAllUsers();
}
import 'package:clean_architecture/domain/entities/user.dart';

abstract class UserRepository{
  Future<List<User>> getAllUsers();
  Future<User> getUser(int id);
  Future<User> updateUser(User user);
  Future<User> deleteUser(int id);
  Future<User> createUser(User user);
}
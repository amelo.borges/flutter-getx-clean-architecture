import 'package:clean_architecture/domain/entities/user.dart';
import 'package:json_annotation/json_annotation.dart';

part 'user_model.g.dart';

@JsonSerializable()
class UserModel extends User{
  String createdAt;
  String name;
  String email;
  String city;
  String avatar;
  String id;
  String birthdate;

  UserModel({
    required this.name,
    required this.avatar,
    required this.email,
    required this.id,
    required this.city,
    required this.createdAt,
    required this.birthdate
  }) : super(name: name, avatar: avatar, email: email, id: id, city: city, createdAt: createdAt, birthdate: birthdate);

  factory UserModel.fromJson(Map<String, dynamic> json) => _$UserModelFromJson(json);
  Map<String, dynamic> toJson() => _$UserModelToJson(this);

  @override
  bool get stringify => true;

  @override
  List<Object?> get props => [
    createdAt,
    name,
    email,
    city,
    avatar,
    id,
    birthdate
  ];



}

// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserModel _$UserModelFromJson(Map<String, dynamic> json) => UserModel(
      name: json['name'] as String,
      avatar: json['avatar'] as String,
      email: json['email'] as String,
      id: json['id'] as String,
      city: json['city'] as String,
      createdAt: json['createdAt'] as String,
      birthdate: json['birthdate'] as String,
    );

Map<String, dynamic> _$UserModelToJson(UserModel instance) => <String, dynamic>{
      'createdAt': instance.createdAt,
      'name': instance.name,
      'email': instance.email,
      'city': instance.city,
      'avatar': instance.avatar,
      'id': instance.id,
      'birthdate': instance.birthdate,
    };

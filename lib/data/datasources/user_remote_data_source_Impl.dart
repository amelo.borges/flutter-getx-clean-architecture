import 'package:clean_architecture/core/utils.dart';
import 'package:clean_architecture/data/datasources/user_remote_data_source.dart';
import 'package:clean_architecture/data/models/user_model.dart';
import 'package:dio/dio.dart';

const String BASE_URL = "https://64172dc89863b4d772a498c4.mockapi.io/";

class UserRemoteDataSourceImpl with Utils implements UserRemoteDataSource{

  final dio = Dio();

  @override
  Future<UserModel> createUser(UserModel user) {
    // TODO: implement createUser
    throw UnimplementedError();
  }

  @override
  Future<UserModel> deleteUser(int id)  {
    // TODO: implement deleteUser
    throw UnimplementedError();
  }

  @override
  Future<List<UserModel>> getAllUsers() async {
    final response = await dio.get('${BASE_URL}users');
    if(response.statusCode ==  200){
      final jsonList = response.data as List<dynamic>;
      return jsonList.map((e) => UserModel.fromJson(e)).toList();
    }
    return [];
  }

  @override
  Future<UserModel> getUser(int id) {
    // TODO: implement getUser
    throw UnimplementedError();
  }

  @override
  Future<UserModel> updateUser(UserModel user) {
    // TODO: implement updateUser
    throw UnimplementedError();
  }

}
import 'package:clean_architecture/data/models/user_model.dart';

abstract class UserRemoteDataSource {
  Future<List<UserModel>> getAllUsers();
  Future<UserModel> getUser(int id);
  Future<UserModel> updateUser(UserModel user);
  Future<UserModel> deleteUser(int id);
  Future<UserModel> createUser(UserModel user);
}
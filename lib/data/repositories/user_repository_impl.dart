import 'package:clean_architecture/data/datasources/user_remote_data_source.dart';
import 'package:clean_architecture/domain/entities/user.dart' as entity;
import 'package:clean_architecture/domain/repositories/user_repository.dart';

class UserRepositoryImpl implements UserRepository{
  final UserRemoteDataSource userRemoteDataSource;

  UserRepositoryImpl({required this.userRemoteDataSource});


  @override
  Future<entity.User> createUser(entity.User user) {
    // TODO: implement createUser
    throw UnimplementedError();
  }

  @override
  Future<entity.User> deleteUser(int id) {
    // TODO: implement deleteUser
    throw UnimplementedError();
  }

  @override
  Future<List<entity.User>> getAllUsers() async {
    return await userRemoteDataSource.getAllUsers();
  }

  @override
  Future<entity.User> getUser(int id) {
    // TODO: implement getUser
    throw UnimplementedError();
  }

  @override
  Future<entity.User> updateUser(entity.User user) {
    // TODO: implement updateUser
    throw UnimplementedError();
  }

}